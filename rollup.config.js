import typescript from 'rollup-plugin-typescript';
import { uglify } from 'rollup-plugin-uglify';

export default {
	input: 'src/dice-debug.ts',
	output: {
		file: 'dist/dice-debug.js',
		format: 'cjs'
	},
	plugins: [typescript(), uglify()]
};