import { DebugPanel, DebugPanelConfig } from './debug-panel';

class DiceDebug extends Phaser.Plugin {
	setup(config: DebugPanelConfig) { new DebugPanel(this.game, this.game.stage, config); }
}

(<any>Phaser.Plugin).DiceDebug = DiceDebug;