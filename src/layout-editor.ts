// TODO rotate
// TODO scale
export class LayoutEditor extends Phaser.Group {
	private history: History;
	private selection: Selection;
	private keyboard: KeyboardInput;
	private lastUpPos = new Phaser.Point();
	private selectionPanel: SelectionPanel;

	public get enabled(): boolean { return this.visible; }
	public set enabled(value: boolean) {
		this.visible = value;
		this.ignoreChildInput = !value;
	}

	constructor(game: Phaser.Game, private root: Phaser.Group) {
		super(game, game.stage, 'Debug Layout');
		this.createGraphics(game);
		this.history = new History();
		this.selectionPanel = new SelectionPanel(game, this);

		this.selection = this.add(new Selection(game, this.history, this.selectionPanel));
		this.selection.onMouseUp.add(this.onMouseUp, this);
		this.keyboard = new KeyboardInput(game, this.selection, this.history);
		this.history.selection = this.selection;
	}

	private createGraphics(game: Phaser.Game) {
		const g = game.add.graphics(0, 0, this);
		g.beginFill(0, 0);
		g.drawRect(0, 0, game.width, game.height);
		g.endFill();
		g.inputEnabled = true;
		g.events.onInputUp.add((_, p) => this.onMouseUp(p, true), this);
	}

	private onMouseUp(pointer: Phaser.Pointer, ignoreLastPos: boolean = false) {
		let selection: PIXI.DisplayObjectContainer;

		if (ignoreLastPos)
			selection = this.select(this.root, pointer.x, pointer.y, null, -1);
		else {
			const last = this.lastUpPos;
			const target = this.selection.target;
			if (target && target.parent && Phaser.Math.distance(last.x, last.y, pointer.x, pointer.y) < 3) {
				selection = this.select(this.root, pointer.x, pointer.y, target.parent, target.parent.getChildIndex(target));
			} else
				selection = this.select(this.root, pointer.x, pointer.y, null, -1);
		}

		this.selection.setSelection(selection);
		this.lastUpPos.set(pointer.x, pointer.y);
	}

	private select(parent: PIXI.DisplayObjectContainer, x: number, y: number, ignoreParent: PIXI.DisplayObjectContainer,
		ignoreChildIndex: number): PIXI.DisplayObjectContainer {
		const children = parent.children;
		const limit = ignoreParent === parent ? ignoreChildIndex : children.length;
		let result = parent;

		for (let i = limit - 1; i >= 0; i--) {
			const child = children[i];
			if (child instanceof PIXI.DisplayObjectContainer) {
				const c = child as PIXI.DisplayObjectContainer;
				if (c.getBounds().contains(x, y)) {
					result = this.select(child as PIXI.DisplayObjectContainer, x, y, ignoreParent, ignoreChildIndex);
					break;
				}
			}
		}
		return result;
	}

	update() {
		if (!this.visible) return;
		super.update();
		this.keyboard.update();
	}
}


/* ============== SELECTION ============== */

class Selection extends Phaser.Graphics {
	public target: PIXI.DisplayObjectContainer;
	private offset = new PIXI.Point();
	private dragging = false;

	public onMouseUp = new Phaser.Signal();

	constructor(game: Phaser.Game, private history: History, private panel: SelectionPanel) {
		super(game);
		this.inputEnabled = true;
		this.input.priorityID = 10;
		this.input.enableSnap(1, 1);
		this.input.enableDrag();
		this.input.dragDistanceThreshold = 4;
		this.events.onDragStart.add(this.onDragStart, this);
		this.events.onInputDown.add(this.onInputDown, this, 10);
		this.events.onDragUpdate.add(this.onDragUpdate, this);
		this.events.onInputUp.add((_, p) => this.onInputUp(p), this);


		let key = game.input.keyboard.addKey(Phaser.KeyCode.SHIFT);
		key.onDown.add(() => this.input.allowHorizontalDrag = false, this);
		key.onUp.add(() => this.input.allowHorizontalDrag = true, this);

		key = game.input.keyboard.addKey(Phaser.KeyCode.CONTROL);
		key.onDown.add(() => this.input.allowVerticalDrag = false, this);
		key.onUp.add(() => this.input.allowVerticalDrag = true, this);

		key = game.input.keyboard.addKey(Phaser.KeyCode.ALT);
		key.onDown.add(this.clear, this);
		key.onUp.add(this.redraw, this);
	}

	private onDragStart() { this.dragging = true; }

	private onInputUp(pointer: Phaser.Pointer) {
		if (!this.dragging) this.onMouseUp.dispatch(pointer);
		this.dragging = false;
	}

	private onInputDown() { this.history.push(this.target); }

	private onDragUpdate() {
		this.target.x = Math.round(this.position.x - this.offset.x);
		this.target.y = Math.round(this.position.y - this.offset.y);
	}

	public setSelection(target: PIXI.DisplayObjectContainer) {
		if (target === this.target) return;
		this.target = target;
		this.redraw();
	}

	private redraw() {
		const target = this.target;
		this.clear();

		this.visible = !!target;
		this.panel.select(target);
		if (!this.visible) return;

		const bounds = target.getBounds();

		this.lineStyle(3, 0x000000, .2);
		this.drawRect(1, 1, bounds.width, bounds.height);

		this.lineStyle(2, 0x2862b9, 1);
		this.drawRect(0, 0, bounds.width, bounds.height);

		this.beginFill(0x000000, 0);
		this.drawRect(0, 0, bounds.width, bounds.height);
		this.endFill();

		const anchor = target.hasOwnProperty('anchor') ? target['anchor'] : new Phaser.Point(0, 0);

		this.beginFill(0xffffff, 1);
		this.lineWidth = 0;
		const c0 = new Phaser.Point(bounds.width * anchor.x, bounds.height * anchor.y);

		let c1 = new Phaser.Point(c0.x, c0.y + 3);
		let c2 = new Phaser.Point(c0.x - 3, c0.y + 10);
		let c3 = new Phaser.Point(c0.x + 3, c0.y + 10);
		this.drawTriangle([c1, c2, c3]);

		c1 = new Phaser.Point(c0.x, c0.y - 3);
		c2 = new Phaser.Point(c0.x - 3, c0.y - 10);
		c3 = new Phaser.Point(c0.x + 3, c0.y - 10);
		this.drawTriangle([c1, c2, c3]);

		c1 = new Phaser.Point(c0.x - 3, c0.y);
		c2 = new Phaser.Point(c0.x - 10, c0.y - 3);
		c3 = new Phaser.Point(c0.x - 10, c0.y + 3);
		this.drawTriangle([c1, c2, c3]);

		c1 = new Phaser.Point(c0.x + 3, c0.y);
		c2 = new Phaser.Point(c0.x + 10, c0.y - 3);
		c3 = new Phaser.Point(c0.x + 10, c0.y + 3);
		this.drawTriangle([c1, c2, c3]);

		// this.lineWidth = 2;
		// this.drawCircle(0, 0, 12);
		// this.drawCircle(0, bounds.height, 12);
		// this.drawCircle(bounds.width, bounds.height, 12);
		// this.drawCircle(bounds.width, 0, 12);

		// const a = { x: bounds.width * .5 - 6, y: bounds.height * .5 - 6 };
		// this.drawRect(a.x, -6, 12, 12);
		// this.drawRect(a.x, bounds.height - 6, 12, 12);
		// this.drawRect(-6, a.y, 12, 12);
		// this.drawRect(bounds.width - 6, a.y, 12, 12);

		this.endFill();

		this.position.set(bounds.x, bounds.y);

		this.offset.x = Math.round(this.position.x - this.target.position.x);
		this.offset.y = Math.round(this.position.y - this.target.position.y);
	}

	public setPosition(x: number, y: number) {
		this.target.x = x;
		this.target.y = y;
		this.position.set(x + this.offset.x, y + this.offset.y);
	}

	public move(x: number, y: number) {
		this.position.x += x;
		this.position.y += y;
		this.target.x += x;
		this.target.y += y;
	}

}


/* ============== KEYBOARD INPUT ============== */

class KeyboardInput {
	private time: Phaser.Time;

	private isMoveKeyDown: boolean;
	private direction = new PIXI.Point();
	private elapsed: number;

	constructor(game: Phaser.Game, private selection: Selection, private history: History) {
		this.time = game.time;

		const k = Phaser.KeyCode;
		const keys = game.input.keyboard.addKeys({
			up: k.UP, down: k.DOWN, left: k.LEFT, right: k.RIGHT,
			clear: k.ESC, undo: k.Z, info: k.I
		});

		keys.up.onDown.add(e => this.moveSelection(e.shiftKey, 0, -1), this);
		keys.up.onUp.add(this.onMoveKeyUp, this);

		keys.down.onDown.add(e => this.moveSelection(e.shiftKey, 0, 1), this);
		keys.down.onUp.add(this.onMoveKeyUp, this);

		keys.left.onDown.add(e => this.moveSelection(e.shiftKey, -1, 0), this);
		keys.left.onUp.add(this.onMoveKeyUp, this);

		keys.right.onDown.add(e => this.moveSelection(e.shiftKey, 1, 0), this);
		keys.right.onUp.add(this.onMoveKeyUp, this);

		keys.clear.onDown.add(() => this.selection.setSelection(null), this);
		keys.undo.onDown.add(e => { if (e.ctrlKey) this.history.undo(); }, this);
		keys.info.onDown.add(this.showInfo, this);
	}

	private moveSelection(shift: boolean, x: number, y: number) {
		this.history.push(this.selection.target);
		this.isMoveKeyDown = true;
		this.elapsed = 0;
		if (shift) {
			x *= 10;
			y *= 10;
		}
		this.direction.set(x, y);
		this.selection.move(x, y);
	}

	private onMoveKeyUp() { this.isMoveKeyDown = false; }
	private showInfo() { if (this.selection.target != null) console.log(this.selection.target); }

	update() {
		if (!this.isMoveKeyDown) return;
		this.elapsed += this.time.elapsedMS;
		if (this.elapsed < 300) return;
		this.selection.move(this.direction.x, this.direction.y);
	}
}


/* ============== HISTORY ============== */

class History {
	private actions: HistoryAction[] = [];
	public selection: Selection;

	public push(object: PIXI.DisplayObjectContainer) {
		if (!object) return;
		if (this.actions.length > 100) this.actions.shift();
		this.actions.push({ object: object, position: { x: object.x, y: object.y } });
	}

	public undo() {
		if (this.actions.length === 0) return;
		const action = this.actions.pop();
		this.selection.setSelection(action.object);
		this.selection.setPosition(action.position.x, action.position.y);
	}
}

class HistoryAction {
	object: PIXI.DisplayObjectContainer;
	position: { x: number, y: number };
}


/* ============== PANEL ============== */

class Panel extends Phaser.Group {
	protected createBackground(x: number, y: number, w: number, h: number, title: string): Phaser.Graphics {
		const g = this.game.add.graphics(x, y, this);
		g.inputEnabled = true;
		g.input.priorityID = 20;
		g.input.enableSnap(1, 1);
		g.input.enableDrag();

		g.beginFill(0x262626);
		g.drawRect(0, 0, w, h);
		g.endFill();

		g.beginFill(0x212121);
		g.drawRect(0, 0, w, 28);
		g.endFill();

		g.addChild(this.createText(15, 5, title, '#aeaeae', 14));
		return g;
	}

	protected createText(x: number, y: number, text: string, color: string, size: number) {
		return this.game.add.text(x, y, text, {
			font: 'Source Code Pro,Consolas,Courier New,monospaced',
			fontSize: size,
			fill: color
		});
	}
}


/* ============== SELECTION PANEL ============== */

class SelectionPanel extends Panel {
	private object: PIXI.DisplayObjectContainer;
	private nameText: Phaser.Text;
	private positionText: Phaser.Text;

	constructor(game: Phaser.Game, parent: PIXI.DisplayObjectContainer) {
		super(game, parent, 'Selection Panel');
		this.visible = false;
		const w = 260;
		const background = this.createBackground(game.width - w - 5, 5, w, 120, 'Properties');
		background.addChild(this.nameText = this.createText(20, 38, '', '#FFF', 14));
		background.addChild(this.positionText = this.createText(20, 82, '', '#FFF', 18));
	}

	public select(object: PIXI.DisplayObjectContainer) {
		this.object = object;
		this.visible = !!object;
		if (this.visible)
			this.nameText.text = 'name: ' + (object['name'] || '-') + '\ntype: ' + object.constructor['name'];
	}

	update() {
		if (!(this.visible && this.object)) return;
		this.positionText.text = `pos: (${this.object.x}, ${this.object.y})`;
	}
}