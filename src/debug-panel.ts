import { LayoutEditor } from './layout-editor';

declare type EventBusType = { emit: (event: string, ...params: any[]) => void };

export interface DebugPanelConfig {
	eventBus?: EventBusType;
	events?: string[];
	previewImages?: { label: string, path: string }[];
}

export class DebugPanel extends Phaser.Group {
	private layoutEditor: LayoutEditor;
	private editLayoutButton: Phaser.Text;
	private muteButton: Phaser.Text;
	private eventBus: EventBusType;

	constructor(game: Phaser.Game, parent: PIXI.DisplayObjectContainer, config: DebugPanelConfig) {
		super(game, parent, 'DebugPanel');
		if (!config) throw Error('No config for Dice Debug');

		this.layoutEditor = new LayoutEditor(game, game.world);
		this.layoutEditor.visible = false;

		const padding = 10;
		const pos = { x: 5, y: 3 };

		this.muteButton = createButton(game, this, '🔊', pos, this.toggleSounds, this);
		pos.x += this.muteButton.width + padding;

		this.editLayoutButton = createButton(game, this, '[EDIT SCENE]', pos, this.toggleLayoutDebug, this);
		pos.x += this.editLayoutButton.width + padding;

		if (config.previewImages) {
			config.previewImages.forEach(image =>
				pos.x += new LayoutImageToggle(game, this, pos, '[' + image.label + ']', image.path).width + padding
			);
		}

		// pos.x += new LayoutImageToggle(game, this, pos, '[MAIN PREV]', imagePath + '/main.jpg').width + padding;
		// pos.x += new LayoutImageToggle(game, this, pos, '[BONUS PREV]', imagePath + '/bonus.jpg').width + padding;

		if (config.events && config.eventBus) {
			this.eventBus = config.eventBus;
			config.events.forEach(event => pos.x += createButton(game, this, event, pos, () => this.eventBus.emit(event), this).width + padding, this);
		}
	}

	private toggleLayoutDebug() {
		this.layoutEditor.enabled = !this.layoutEditor.enabled;
		toggleButton(this.editLayoutButton, this.layoutEditor.enabled);
	}

	private toggleSounds() {
		const sound = this.game.sound;
		sound.mute = !sound.mute;
		this.muteButton.text = sound.mute ? '🔈' : '🔊';
	}
}


class LayoutImageToggle extends Phaser.Group {
	private enabled: boolean;
	private image: Phaser.Image;
	private label: Phaser.Text;

	constructor(game: Phaser.Game, parent: PIXI.DisplayObjectContainer, pos: { x: number, y: number }, text: string, private path: string) {
		super(game, parent);
		this.label = createButton(game, this, text, pos, this.toggleImage, this);
	}

	private toggleImage() {
		this.enabled = !this.enabled;
		toggleButton(this.label, this.enabled);

		if (!this.enabled) {
			if (this.image) this.image.visible = false;
			return;
		}

		if (this.image) {
			this.image.visible = true;
			return;
		}

		const realPath = this.path;
		const loader = this.game.load.image(this.path, realPath);
		loader.onLoadComplete.addOnce(() => {
			if (!this.game.cache.checkImageKey(this.path)) {
				this.toggleImage();
				console.warn(`Preview image not found at '${realPath}'`);
				return;
			}
			this.parent.addChildAt(this.image = this.game.add.image(0, 0, this.path), 0);
			this.image.alpha = .6;
			this.image.visible = true;
		}, this);
		loader.start();
	}
}

function createButton(game: Phaser.Game, parent: Phaser.Group, text: string, pos: { x: number, y: number }, callback: Function, context: any) {
	const button = game.add.text(pos.x, pos.y, text, {
		font: '14px Source Code Pro,Consolas,Courier New,monospaced',
		fill: '#FFF',
		stroke: '#444',
		strokeThickness: 1
	}, parent).setShadow(0, 0, 'black', 2);
	button.inputEnabled = true;
	button.input.priorityID = 1000;
	button.input.useHandCursor = true;
	button.events.onInputUp.add(callback, context);
	button.events.onInputOver.add(() => button.shadowBlur = 5);
	button.events.onInputOut.add(() => button.shadowBlur = 2);
	return button;
}

function toggleButton(button: Phaser.Text, state: boolean) { button.fill = state ? '#FF0' : '#FFF'; }